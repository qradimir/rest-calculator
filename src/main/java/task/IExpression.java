package task;

import java.util.Map;

/**
 * {@code IExpression} interface
 */
public interface IExpression {

    /**
     * Evaluates the expression by given variable values
     *
     * @param variables variable values that used in the expression
     * @return result of evaluating
     * @throws NullPointerException  if some of variable values that used in
     *         the expression is missing
     * @throws ArithmeticException if expression can't be evaluated
     */
    double evaluate(Map<String, Double> variables);
}
