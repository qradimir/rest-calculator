package task;

import java.util.*;
import java.util.function.Function;
import java.util.function.Supplier;

import static org.junit.Assert.assertEquals;

final class TestEntry {

    static Map<String, Double> VARIABLES;

    static final Iterator<Double> DOUBLES = new Random().doubles().iterator();

    static final List<TestEntry> VAR_TESTS = new ArrayList<>();
    static final List<TestEntry> NO_VAR_TESTS = new ArrayList<>();
    static {
        //constant
        addTest("4.0", 4d);
        addTest("0.4", 0.4d);
        addTest(".4", 0.4d);
        addTest("12.34", 12.34d);
        addTest("0.2+.2", 0.4d);
        addTest(".2+0.2", 0.4d);
        addTest("5.20/2.600+.0001", 2.0001d);
        //variables
        addTest("x", TestEntry::x);
        //operations
        addTest("x+y", () -> x() + y());
        addTest("y*z", () -> y() * z());
        addTest("z/x", () -> z() / x());
        addTest("-z", () -> -z());
        //priority
        addTest("x+y*z", () -> x() + y() * z());
        addTest("(x+y)*z", () -> (x() + y()) * z());
        addTest("-x+z", () -> -x() + z());
        addTest("-(x+z)", () -> -(x() + z()));
    }
    private static void addTest(String expression, Supplier<Double> checker) {
        VAR_TESTS.add(new TestEntry(expression, checker));
    }
    private static void addTest(String expression, Double result) {
        NO_VAR_TESTS.add(new TestEntry(expression, () -> result));
    }
    private static double x() {
        return VARIABLES.get("x");
    }
    private static double y() {
        return VARIABLES.get("y");
    }
    private static double z() {
        return VARIABLES.get("z");
    }

    private final String expression;
    private final Supplier<Double> expected;

    private TestEntry(String expression, Supplier<Double> expected) {
        this.expression = expression;
        this.expected = expected;
    }

    void test(Function<String, Double> actual) {
        assertEquals(expected.get(), actual.apply(expression), 1E-06);
    }
}
