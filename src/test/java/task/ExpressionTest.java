package task;

import org.junit.Test;

import java.util.HashMap;
import java.util.function.Function;

import static task.TestEntry.VARIABLES;

public final class ExpressionTest {

    private static final int TEST_CYCLES = 5;
    private static final Function<String, Double> EVALUATOR =
            (str) -> Expression.valueOf(str).evaluate(VARIABLES);

    @Test
    public void NonVariableTest() {
        TestEntry.NO_VAR_TESTS.forEach((entry -> entry.test(EVALUATOR)));
    }

    @Test
    public void VariableTest() {
        VARIABLES = new HashMap<>();
        for (int i = 0; i < TEST_CYCLES; i++) {
             VARIABLES.put("x", TestEntry.DOUBLES.next());
             VARIABLES.put("y", TestEntry.DOUBLES.next());
             VARIABLES.put("z", TestEntry.DOUBLES.next());
            TestEntry.VAR_TESTS.forEach((entry) -> entry.test(EVALUATOR));
        }
    }
}
