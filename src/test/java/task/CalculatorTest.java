package task;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.URI;

import static task.TestEntry.*;

import static org.junit.Assert.assertEquals;

public class CalculatorTest {

    private final static String serverUri = "http://localhost:8080";

    private CalculatorServer server;
    private CalculatorClient client;

    @Before
    public void start() throws Exception {
        server = new CalculatorServer(URI.create(serverUri));
        client = CalculatorClient.instance(serverUri);
        VARIABLES = server.getVariables();
    }

    @After
    public void shutdown() throws Exception {
        server.close();
    }

    private double fetchSetVariable(String var, String expr) throws IOException {
        return client.setVariable(var, expr).execute().body();
    }

    private double fetchGetVariable(String var) throws IOException {
        return client.getVariable(var).execute().body();
    }

    private double fetchEvaluate(String expr) throws IOException {
        return client.evaluate(expr).execute().body();
    }

    @Test
    public void twoPlusTwoTest() throws IOException {
        assertEquals(4d, fetchEvaluate("2+2"), 1E-06);
    }

    @Test
    public void VariablePostingTest() throws IOException {
        assertEquals(Double.NaN, fetchGetVariable("x"), 1E-06);

        assertEquals(4d, fetchSetVariable("x", "2+2"), 1E-06);

        assertEquals(4d, server.getVariable("x"), 1E-06);
        assertEquals(4d, fetchGetVariable("x"), 1E-06);
    }

    @Test
    public void VariableUsingTest() throws IOException {
        for (TestEntry entry: VAR_TESTS) {
            fetchSetVariable("x", DOUBLES.next().toString());
            fetchSetVariable("y", DOUBLES.next().toString());
            fetchSetVariable("z", DOUBLES.next().toString());

            entry.test((expr) -> {
                try {
                    return fetchSetVariable("result", expr);
                } catch (IOException ignored) {
                    return null;
                }
            });
        }
    }

}
