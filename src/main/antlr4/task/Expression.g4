grammar Expression;

expression returns[Expression e]
    : left=multExpression '+' right=expression { $e = Expression.add($left.e, $right.e); }
    | left=multExpression '-' right=expression { $e = Expression.subtract($left.e, $right.e); }
    | multExpression { $e = $multExpression.e; }
    ;

multExpression returns[Expression e]
    : left=atom '*' right=multExpression { $e = Expression.product($left.e, $right.e); }
    | left=atom '/' right=multExpression { $e = Expression.divide($left.e, $right.e); }
    | atom { $e = $atom.e; }
    ;

atom returns[Expression e]
    : dbl { $e = Expression.constant($dbl.d); }
    | ID { $e = Expression.variable($ID.text); }
    | '-' r=atom { $e = Expression.negate($r.e); }
    | '(' expression ')' { $e = $expression.e; }
    ;

dbl returns[double d]
    : a=INT '.' b=INT { $d = Double.valueOf($a.text + "." + $b.text); }
    | '.' INT { $d = Double.valueOf("0." + $INT.text); }
    | INT { $d = Double.valueOf($INT.text); }
    ;

ID  : [a-zA-Z]+;
INT : [0-9]+;
