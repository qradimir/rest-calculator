package task;


import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;

import java.util.Map;
import java.util.Objects;

/**
 * Base {@link IExpression} class.
 * <p>
 * Contains lots of {@code Expression} factories, like:
 * @see #valueOf(String)
 * @see #constant(double)
 * @see #variable(String)
 * @see #add(Expression, Expression)
 * @see #subtract(Expression, Expression)
 * @see #product(Expression, Expression)
 * @see #divide(Expression, Expression)
 * @see #negate(Expression)
 */
public abstract class Expression implements IExpression {

    protected final int priority;

    public Expression(int priority) {
        this.priority = priority;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public abstract String toString();

    /**
     * Returns a string representation that bounded with braces if
     * given <tt>priority</tt> is upper then the expression priority
     *
     * @return a string representation
     */
    private String toString(int priority) {
        return this.priority < priority ? "(" + toString() + ")" : toString();
    }

    public static Expression valueOf(String str) {
        ExpressionLexer lexer = new ExpressionLexer(new ANTLRInputStream(str));
        return new ExpressionParser(new CommonTokenStream(lexer)).expression().e;
    }


    /**
     * Creates expression that result of evaluation is a provided value of
     * <tt>name</tt> variable
     *
     * @param name variable name
     * @return expression that result of evaluation is a provided value of
     * <tt>name</tt> variable
     */
    public static Expression variable(String name) {
        Objects.requireNonNull(name);
        return new Expression(5) {
            @Override
            public double evaluate(Map<String, Double> variables) {
                return Objects.requireNonNull(variables.get(name), "No value has set");
            }

            @Override
            public String toString() {
                return name;
            }
        };
    }

    /**
     * Creates expression that result of evaluation is always <tt>value</tt>.
     *
     * @param value a value
     * @return expression that result of evaluation is always <tt>value</tt>
     */
    public static Expression constant(double value) {
        return new Expression(5) {
            @Override
            public double evaluate(Map<String, Double> variables) {
                return value;
            }

            @Override
            public String toString() {
                return Double.toString(value);
            }
        };
    }

    /**
     * Creates expression that evaluates a addition of given expressions
     *
     * @param left left argument of addition
     * @param right right argument of addition
     * @return expression that evaluates a addition of given expressions
     */
    public static Expression add(Expression left, Expression right) {
        Objects.requireNonNull(left);
        Objects.requireNonNull(right);
        return new Expression(1) {
            @Override
            public double evaluate(Map<String, Double> variables) {
                return left.evaluate(variables) + right.evaluate(variables);
            }

            @Override
            public String toString() {
                return left.toString(this.priority) + "+" +
                        right.toString(this.priority);
            }
        };
    }

    /**
     * Creates expression that evaluates a subtraction of given expressions
     *
     * @param left left argument of subtraction
     * @param right right argument of subtraction
     * @return expression that evaluates a subtraction of given expressions
     */
    public static Expression subtract(Expression left, Expression right) {
        Objects.requireNonNull(left);
        Objects.requireNonNull(right);
        return new Expression(1) {
            @Override
            public double evaluate(Map<String, Double> variables) {
                return left.evaluate(variables) + right.evaluate(variables);
            }

            @Override
            public String toString() {
                return left.toString(priority) + "-" +
                        right.toString(priority + 1);
            }
        };
    }

    /**
     * Creates expression that evaluates a production of given expressions
     *
     * @param left left argument of production
     * @param right right argument of production
     * @return expression that evaluates a production of given expressions
     */
    public static Expression product(Expression left, Expression right) {
        Objects.requireNonNull(left);
        Objects.requireNonNull(right);
        return new Expression(2) {
            @Override
            public double evaluate(Map<String, Double> variables) {
                return left.evaluate(variables) * right.evaluate(variables);
            }

            @Override
            public String toString() {
                return left.toString(priority) + "*" +
                        right.toString(priority);
            }
        };
    }

    /**
     * Creates expression that evaluates a division of given expressions
     *
     * @param left left argument of division
     * @param right right argument of division
     * @return expression that evaluates a division of given expressions
     */
    public static Expression divide(Expression left, Expression right) {
        Objects.requireNonNull(left);
        Objects.requireNonNull(right);
        return new Expression(2) {
            @Override
            public double evaluate(Map<String, Double> variables) {
                return left.evaluate(variables) / right.evaluate(variables);
            }

            @Override
            public String toString() {
                return left.toString(priority) + "/" +
                        right.toString(priority + 1);
            }
        };
    }

    /**
     * Creates expression that evaluates a negation of given expression
     *
     * @param expr a expression
     * @return expression that evaluates a negation of given expression
     */
    public static Expression negate(Expression expr) {
        Objects.requireNonNull(expr);
        return new Expression(2) {
            @Override
            public double evaluate(Map<String, Double> variables) {
                return -expr.evaluate(variables);
            }

            @Override
            public String toString() {
                return "-" + expr.toString(priority);
            }
        };
    }
}
