package task;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.server.ResourceConfig;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.net.URI;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory.createHttpServer;

/**
 * {@code CalculatorServer} holding a REST-server that listening for request:
 * <p>
 * <t>GET   calculate/{var}</t> return value of <t>var</t> variable
 * <t>POST  calculate/{var}?expr={expression}</t> evaluates <t>expression</t> and
 *          assign <t>var</t> variable with evaluated result
 * <t>GET   calculate?expr={expression}</t> evaluates <t>expression</t>
 * </p>
 */
@Path("calculate")
public class CalculatorServer implements AutoCloseable {

    private final Map<String, Double> variables = new ConcurrentHashMap<>();
    private final HttpServer server;

    /**
     * Creates and starts {@code CalculatorServer}.
     *
     * @param uri {@link URI} on which {@code CalculatorServer} will be started.
     */
    public CalculatorServer(URI uri) {
        this.server = createHttpServer(uri, new ResourceConfig().register(this));
    }

    /**
     * Returns value of <t>var</t> variable.
     *
     * @param var name of variable
     * @return value of variable
     */
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("{var}")
    public double getVariable(@PathParam("var") String var) {
        return variables.getOrDefault(var, Double.NaN);
    }

    /**
     * Returns map of variables and their values that the server holding.
     *
     * @return server variables
     */
    public Map<String, Double> getVariables() {
        return Collections.unmodifiableMap(variables);
    }

    /**
     * Evaluates <t>expr</t> and assigns <t>var</t> variable with result of evaluation.
     * In fact, <t>var</t> variable will be assign with
     * {@code Expression.valueOf(expr).evaluate(getVariables())}
     *
     * @param var name of variable
     * @param expr a expression
     * @return new value of variable
     */
    @POST
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    @Path("{var}")
    public double setVariable(@PathParam("var") String var, String expr) {
        double res = Expression.valueOf(expr).evaluate(variables);
        variables.put(var, res);
        return res;
    }

    /**
     * Evaluates <t>expr</t>
     * In fact, runs {@code Expression.valueOf(expr).evaluate(getVariables())}
     *
     * @param expr a expression
     * @return result of evaluation
     */
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public double evaluate(@QueryParam("expr") String expr) {
        return Expression.valueOf(expr).evaluate(variables);
    }

    /**
     * Closes the server
     */
    @Override
    public void close() {
        server.shutdownNow();
    }
}
