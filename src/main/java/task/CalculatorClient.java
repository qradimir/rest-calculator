package task;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import retrofit2.http.*;

/**
 * {@code CalculatorClient} provides methods for fetching requests
 * to {@link CalculatorServer}.
 * <p>
 * You can you {@link CalculatorClient#instance(String)} to get default
 * implementation that provided by {@link retrofit2} framework.
 * </p>
 */
public interface CalculatorClient {

    /**
     * Requests for <t>expression</t> evaluation
     *
     * @param expression what server will evaluate
     * @return result of evaluation
     */
    @GET("calculate")
    Call<Double> evaluate(@Query("expr") String expression);

    /**
     * Requests for <t>var</t> variable value.
     *
     * @param var name of variable
     * @return value of variable
     */
    @GET("calculate/{var}")
    Call<Double> getVariable(@Path("var") String var);

    /**
     * Requests for assigning <t>var</t> variable value with result of
     * <t>expression</> evaluation.
     *
     * @param var name of variable
     * @param expression what server will evaluate
     * @return new value of variable
     */
    @POST("calculate/{var}")
    Call<Double> setVariable(@Path("var") String var, @Body String expression);


    /**
     * Creates implementation of the interface.
     * Uses {@link retrofit2} framework to create instance.
     *
     * @param url address of server
     * @return instance of the interface
     */
    static CalculatorClient instance(String url) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();
        return retrofit.create(CalculatorClient.class);
    }
}
